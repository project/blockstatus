(function ($) {

/**
 * Provide the summary information for the blockstatus settings vertical tabs.
 */
Drupal.behaviors.blockstatusSettingsSummary = {
  attach: function (context) {
    $('fieldset.blockstatus-form-options', context).drupalSetSummary(function (context) {
      var vals = [];

      $('input:checked ~ label', context).each(function () {
        vals.push(Drupal.checkPlain($.trim($(this).text())));
      });

      if (!$('.form-item-status input', context).is(':checked')) {
        vals.unshift(Drupal.t('Not published'));
      }
      return vals.join(', ');
    });
  }
};
})(jQuery);
