Block Status
============

This module adds a status-flag to blocks. Using this flag it is possible to
specify whether a block should be published or not. Users with the appropriate
permission may access unpublished blocks.
